# Encuesta Musical 3IT

<p align="center">
    <img src="./angular-encuesta/images/encuesta-angular.png">
</p>


## Version Backend
-   Spring Boot 2.7.9
-   Swagger 2.9.2

## Version Frontend
-   Angular 15.2.1
-   Angular Material 14.2.7

## Requerimientos Minimos 
-   Java 11
-   Maven 3.0+
-   NodeJS 14.20.x, 16.13.x ó 18.10.x

## Database
- In-Memory Database

## Deploy Backend

1.   Descargar proyecto 
 
2.   Abrir terminal en la ruta principal del proyecto

3.   Ingresar en la ruta \api-encuesta y ejecutar el siguiente comando

            mvn spring-boot:run

## Deploy Frontend

1.   Descargar proyecto 
 
2.   Abrir terminal en la ruta principal del proyecto

3.   Ingresar en la ruta \angular-encuesta y ejecutar los siguientes comandos

            npm install
            ng serve --open


## Testing Backend

1. una vez ejecutado el sistema se genera documentacion y testing con Swagger, se puede consultar a traves de la URL http://localhost:8080/swagger-ui.html

2. Se anexa en la ruta principal del proyecto archivo para importacion de prueba de endpoints y Test en programa POSTMAN .

3. Se pueden realizar pruebas unitarias con JUnit en el archivo EncuestaMusicalApplicationTests.java.

## Intrucciones

### Se requiere desarrollar una aplicación para encuesta de estilo de música que escucha.:

-	 El stack de herramientas es SpringBoot(JAVA), Angular y la base de datos a su preferencia
-	 El formulario de encuesta tiene los campos:
        -    Mail (único e identificador del usuario)
        -    Rock, Pop , JAZZ, CLASICA , etc
-	 Luego de esta respuesta se le debe dar el feedback al usuario que fue exitosamente o hubo errores en el formulario
-	 Finalmente se debe mostrar una tabla con todos los resultados 
-	 Test unitario por capa.
-	 Aplicar patrones de diseño tanto para capa frontend como backend.
-	 Se revisará códigos por Sonarqube.

## Consideraciones

-	Deseable integración de framework de presentación como Material o Bootstrap.
-	Buenas prácticas de codificación para frontend y backend.
-	Incluir un README para la instalación y configuración
-	De preferencia ocupar In-Memory Database( para evitar tiempo en ambientación)


