package cl.erickbalda.encuestamusical;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.erickbalda.encuestamusical.model.dto.EncuestaDTO;
import cl.erickbalda.encuestamusical.repository.EstiloMusicalRepository;

@SpringBootTest
@AutoConfigureMockMvc
class EncuestaMusicalApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    EstiloMusicalRepository estiloMusicalRepository;

    @Test
    void getAllEncuestasTest() throws Exception {
        this.mockMvc.perform(get("/v1/api/encuesta")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void getAllEstilosMusicalTest() throws Exception {
        this.mockMvc.perform(get("/v1/api/estilomusical")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void saveEncuestaTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/api/encuesta")
                .content(asJsonString(new EncuestaDTO("test@test.com", 1)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
