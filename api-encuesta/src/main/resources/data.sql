DROP TABLE IF EXISTS estilo_musical;

DROP TABLE IF EXISTS encuesta;


CREATE TABLE estilo_musical (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  estilo VARCHAR(250) NOT NULL
);

CREATE TABLE encuesta (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  email  VARCHAR(250) NOT NULL UNIQUE,
  id_estilo INT NOT NULL
);

INSERT INTO estilo_musical (estilo) VALUES
  ('Rock'),
  ('Jazz'),
  ('Pop'),
  ('Country'),
  ('KPop'),
  ('Dance');

  INSERT INTO encuesta (email, id_estilo) VALUES
  ('a@test.com', 1);
