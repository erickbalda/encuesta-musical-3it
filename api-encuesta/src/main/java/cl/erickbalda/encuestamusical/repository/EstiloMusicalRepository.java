package cl.erickbalda.encuestamusical.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.erickbalda.encuestamusical.model.EstiloMusical;

public interface EstiloMusicalRepository extends JpaRepository<EstiloMusical, Integer>{
    
}
