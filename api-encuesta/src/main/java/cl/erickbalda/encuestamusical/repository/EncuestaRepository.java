package cl.erickbalda.encuestamusical.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.erickbalda.encuestamusical.model.Encuesta;

public interface EncuestaRepository extends JpaRepository<Encuesta, Integer>{
    
}
