package cl.erickbalda.encuestamusical.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.erickbalda.encuestamusical.model.Encuesta;
import cl.erickbalda.encuestamusical.model.EstiloMusical;
import cl.erickbalda.encuestamusical.model.dto.EncuestaDTO;
import cl.erickbalda.encuestamusical.repository.EncuestaRepository;
import cl.erickbalda.encuestamusical.repository.EstiloMusicalRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("v1/api")
public class EncuestaMusicalController {

    @Autowired
    EncuestaRepository encuestaRepository;

    @Autowired
    EstiloMusicalRepository estiloMusicalRepository;

    /**
     * Listado de resultado encuestas
     */
    @GetMapping("/encuesta")
    public ResponseEntity<List<Encuesta>> getAllEncuestas() {
        try {
            List<Encuesta> encuesta = encuestaRepository.findAll();
            if (encuesta.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(encuesta, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Guarda nueva encuesta
     */
    @PostMapping("/encuesta")
    public ResponseEntity<Encuesta> saveEncuesta(@Valid @RequestBody EncuestaDTO encuesta) {
        try {
            Encuesta a = new Encuesta(0, encuesta.getEmail(), estiloMusicalRepository.getReferenceById(encuesta.getEstiloId()));
            encuestaRepository.save(a);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Listado de resultado encuestas
     */
    @GetMapping("/estilomusical")
    public ResponseEntity<List<EstiloMusical>> getAllEstilosMusical() {
        try {
            List<EstiloMusical> estilo = estiloMusicalRepository.findAll();
            if (estilo.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(estilo, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

