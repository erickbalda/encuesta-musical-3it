package cl.erickbalda.encuestamusical.model.dto;


public class EncuestaDTO {

     String email;
     Integer estiloId;
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Integer getEstiloId() {
        return estiloId;
    }
    public void setEstiloId(Integer estiloId) {
        this.estiloId = estiloId;
    }
    @Override
    public String toString() {
        return "EncuestaDTO [email=" + email + ", estiloId=" + estiloId + "]";
    }
    public EncuestaDTO(String email, Integer estiloId) {
        this.email = email;
        this.estiloId = estiloId;
    }    
    
}