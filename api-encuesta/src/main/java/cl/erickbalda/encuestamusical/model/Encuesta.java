package cl.erickbalda.encuestamusical.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "encuesta")
public class Encuesta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "email", nullable = false)
    private String email;

    
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_estilo")
    private EstiloMusical estiloMusical;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EstiloMusical getEstiloMusical() {
        return estiloMusical;
    }

    public void setEstiloMusical(EstiloMusical estiloMusical) {
        this.estiloMusical = estiloMusical;
    }

    @Override
    public String toString() {
        return "Encuesta [email=" + email + ", estiloMusical=" + estiloMusical + ", id=" + id + "]";
    }

    public Encuesta(int id, String email, EstiloMusical estiloMusical) {
        this.id = id;
        this.email = email;
        this.estiloMusical = estiloMusical;
    }

    public Encuesta() {
    }

    
}