import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { EncuestaResolver } from './components/encuesta/resolver/encuesta.resolver';
import { EncuestaService } from './components/encuesta/services/encuesta.service';

const routes: Routes = [
  {
    path: '',
    component: EncuestaComponent,
    resolve: {
        initData: EncuestaResolver,
    },
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [EncuestaResolver, EncuestaService]
})
export class AppRoutingModule {}
