import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EncuestaService } from '../services/encuesta.service';

@Injectable()
export class EncuestaResolver {
  constructor(
        private service: EncuestaService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
      return this.service.loadEstilos();
  }
}
