import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EncuestaService {
  constructor(private http: HttpClient) {}

  /**
   * Obtiene los estilos de musica para la carga del componente Select
   *
   */
  loadEstilos(): Observable<any> {
    return this.http.get('http://localhost:8080/v1/api/estilomusical');
  }

  

  /**
   * Guarda los datos de la encuesta 
   * del usuario
   * @param email correo ingresado por el usuario
   * @param id_estilo codigo del estilo musical seleccionado por el usuario
   */
  saveEncuesta(email: any, id_estilo: any): Observable<any> {
    const body = {
      email: email,
      estiloId: id_estilo,
    };
    return this.http.post<any>('http://localhost:8080/v1/api/encuesta', body);
  }

  /**
   * Obtiene los resultados de la encuesta
   *
   */
  loadResultados(): Observable<any> {
    return this.http.get('http://localhost:8080/v1/api/encuesta');
  }



}
