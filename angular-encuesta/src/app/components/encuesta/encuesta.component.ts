import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { EncuestaService } from './services/encuesta.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css'],
})
export class EncuestaComponent implements OnInit {
  encuestaForm: UntypedFormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: UntypedFormBuilder,
    private encuestaService: EncuestaService,
    private _snackBar: MatSnackBar
  ) {
    this.encuestaForm = new UntypedFormGroup({
      email: new UntypedFormControl('', [Validators.required, Validators.email]),
      estilo: new UntypedFormControl('', [Validators.required]),
    });
  }

  datosEstilos: any;
  resultados: any;
  mensaje: any;
  displayedColumns: string[] = ['id', 'email', 'estilo'];

  ngOnInit(): void {
    this.datosEstilos = this.route.snapshot.data['initData'];
  }

  /**
   * Proceso de creación de suscripción o cambio de basica a avanzada
   */
  saveEncuesta() {
    this.encuestaService
      .saveEncuesta(
        this.encuestaForm.value['email'],
        this.encuestaForm.value['estilo']
      )
      .subscribe(
        (sav) => {
          this.mensaje = 'Suscripcion exitosa';
          this.openSnackBar(this.mensaje, 'Cerrar');
          this.loadResultados();
        },
        (errorb) => {
        console.log(errorb);
          this.mensaje = 'Su correo ya se encuentra registrado ';
          this.openSnackBar(this.mensaje, 'Cerrar');
        }
      );
  }

  /**
   * Proceso de creación de suscripción o cambio de basica a avanzada
   */
  loadResultados() {
    this.encuestaService.loadResultados().subscribe(
      (res) => {
        this.resultados = res;
      },
      (errora) => {
        console.log(errora);
      }
    );
  }

  /**
   * Metodo para la creacion de snackBar
   */
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  /**
   * Metodo para la verificacion de input mail
   */
  getErrorMessage() {
    if (this.encuestaForm.controls.email.hasError('required')) {
      return 'Debe ingresar su correo';
    }
    return this.encuestaForm.controls.email.hasError('email')
      ? 'Formato de correo no valido '
      : '';
  }
}
